p:=2;
r:=2;
s:=2;
m:=7;
t:=2;



n:=8;
F:=GF(p^m);
a:=Generator(F);


function qprod(q,t,r)
if r in [1..t] then 
return &*{*q^t-q^j:j in [0..r-1]*};
elif
r eq 0 then
return 1;
else   
return 0;   
end if;
end function;

function qbin(q,n,r) 
if r le -1 then
ans:=0;  
elif
q eq 1 then
ans:=Binomial(n,r);
else   
ans:=qprod(q,n,r)/qprod(q,r,r);
end if;
return ans;
end function;

function qpdet(q,r)
R:=#r;
A:=&*{* q^r[j]-q^r[i]: i,j in [1..R]| i le j-1 *};
B:=&*{* q^r[j]-1: j in [1..R]*};
C:=q^(&+{* Binomial(j,2): j in [1..R] *});
D:=&*{* (q^j-1)^(R-j+1): j in [1..R]*};
ans:=A*B/(C*D);
return ans;
end function;

function qpdet0(q,r)
R:=#r;
A:=&*{* q^r[j]-q^r[i]: i,j in [1..R]| i le j-1 *};
B:=q^Binomial(R,2);
C:=&*{* q^j-q^i:i,j in [1..R]| i le j-1*};
ans:=A*B/C;
return ans;
end function;

function qpascalmat(q,n,r)
R:=#r;
L:=[[qbin(q,n-r[i],j): i in [1..R]]: j in [1..R]];
M:=Matrix(R,R,L);
return M;
end function;

function qpascalmat0(q,n,r)
R:=#r;
L:=[[qbin(q,n-r[i],j-1): i in [1..R]]: j in [1..R]];
M:=Matrix(R,R,L);
return M;
end function;

qpascal1:=function(q,n)
L:=[[qbin(q,n-i,j): i in [0..n]]: j in [0..n]];
M:=Matrix(Rationals(),n+1,n+1,L);
return M;
end function;

qpascal2:=function(q,n,m,k)
L:=[[q^(m*(n-k-j))*qbin(q,n-i,j-i): i in [0..n]]: j in [0..n]];
M:=Matrix(Rationals(),n+1,n+1,L);
return M;
end function;

//////////////////////////////////////

function qpascal3(q,n)
L:=[[qbin(q,n-i,j-i): i in [0..n]]: j in [0..n]];
M:=Matrix(Rationals(),n+1,n+1,L);
return M;
end function;

function qpascal4(q,n,m)
L:=[[q^(m*(j))*qbin(q,n-i,j): i in [0..n]]: j in [0..n]];
M:=Matrix(Rationals(),n+1,n+1,L);
return M;
end function;

/////computes the rank weight enumerator of dual code/////////////

function macwill(q,n,m,k,L)
Lvec:=Matrix(Rationals(),#L,1,L);
mat1:=qpascal4(q,n,m);
mat2:=qpascal3(q,n);
ans:=q^(-k)*mat2^(-1)*mat1*Lvec;
return ans;
end function;


function distance(weights, len)
  //returns the distance given the weights of the rank metric code
  for i in [2..len] do
     if weights[i] ne 0 then 
        return i-1; 
     end if; 
  end for;       
  return -1; 
end function;     

function first_nonzero_entry(codeword, len)
    //returns the index of the first non-zero entry in the weights list
    for i in [1..len] do
     if codeword[i] ne 0 then 
        return i; 
     end if; 
  end for;       
  return -1; 
end function;


function check_theorem(d_weights,distance,len,t)
    counter:=0;

    if distance le t then
        return false;
    end if;
    for i in [2..len] do
        if (d_weights[i] ne 0) then
            counter+:=1;
        end if;
    end for;
    if (counter gt (distance-t)) then
        return false;
    end if;
    return true;
end function;

function check_weight_distribution(Cd,t,N,m,p,field_size)
   //this function checks if the condition on the weight distributions of the dual code are verified. It implements several tricks to compute all weights fast.
    
    
    length:=Minimum(m,N);
    max:=Maximum(m,N);
    weights:=[0 : i in [1..length+1]];
	w_counter:=0;
    mrd:=false;
	
    dim:=Dimension(Cd);
    weights[1]:=1;
	for c in Cd do

      
	   index:= first_nonzero_entry(c,N);
	   if ((index ne -1) and (c[index] eq 1)) then
	      	      
		  Mat:=Matrix(N,m,[ElementToSequence(c[k],GF(p)): k in [1..N]]);
		  i:=Rank(Mat);
                  if ((i le N-t) and (i ge 1) and (weights[i+1] eq 0)) then
		             w_counter+:=1;
                  end if; 
		  weights[i+1]+:=field_size;

	    else 
		    continue; 
	    end if;      
     end for; 
     d_weights:=macwill(p,m,N,dim*m,weights);
     d:=distance(weights,#weights);
     if (dim*m eq max*(length-d+1)) then
        mrd:=true;
     end if;
     l:=Minimum(N+1-t,length+1);
     check:=check_theorem(d_weights,d,l,t);
     l1:=Minimum(N, length+1);
     triv_design:=check_theorem(d_weights,d,l1,1);
     //print "weights,d_weights", weights, d_weights, mrd, triv_design;

    return check,weights,d_weights,mrd,triv_design;
end function;
	  
enum:=[0: i in [1..m*s]];

counter:=1;


T:=CartesianPower(F,s*(n-s)-1);
Id:=IdentityMatrix(GF(p^m),s);

counter:=1;
field_size:=p^m-1;
time_s:=Cputime();
for tuple in T do
   mat:=[g : g in TupleToList(tuple)];
   Append(~mat,a);
   Y:=Matrix(s,n-s,mat);
   H:=HorizontalJoin(Id,Y);
   Cd:=LinearCode(H);
   check,weights,d_weights,mrd,triv_design:=check_weight_distribution(Cd,t,n,m,p,field_size);
   if (counter mod 100000 eq 0) then
         print "counter", counter;
   end if;
   if check then
        SetOutputFile("good_codes_n8_m7_t2_s2.m");
        print "found matrix, weights, d_weights", Y, weights, d_weights;
        UnsetOutputFile();
   end if;
   if mrd then
        SetOutputFile("mrd_codes_n8_m7_t2_s2.m");
        print "found matrix, weights, d_weights", Y, weights, d_weights;
        UnsetOutputFile();
    end if;
   if triv_design then
        SetOutputFile("triv_design_codes_n8_m7_t2_s2.m");
        print "found matrix, weights, d_weights", Y, weights, d_weights;
        UnsetOutputFile();
   end if;
end for;
print "finished first set of tests", Cputime(time_s);
   


 
